#!/usr/bin/env python3
import os, sys
import ROOT
import math 
import numpy as np 

class variables(object):
    def __init__(self, name):

        #Create file 
        input_file = name
        output_file_name = os.path.basename(str(input_file)).split(".root", 1)[0]+"_Skim.root"
        output_file_dir = "./"
        if not os.path.exists(output_file_dir): os.makedirs(output_file_dir)
        compression = "LZMA:9"
        ROOT.gInterpreter.ProcessLine("#include <Compression.h>")
        (algo, level) = compression.split(":")
        compressionLevel = int(level)
        if   algo == "LZMA": compressionAlgo  = ROOT.ROOT.kLZMA
        elif algo == "ZLIB": compressionAlgo  = ROOT.ROOT.kZLIB
        else: raise RuntimeError("Unsupported compression %s" % algo)
        self.outputfile = ROOT.TFile(output_file_dir+output_file_name, 'RECREATE',"",compressionLevel)
        self.outputfile.SetCompressionAlgorithm(compressionAlgo)

        #All entries 
        self.evtree = ROOT.TTree('evtree', 'evtree')
        self.add_float(self.evtree, "sumNumEvt")
        self.add_float(self.evtree, "sumgenWeight")
        
        #Common variables 
        self.Events = ROOT.TTree('Events', 'Events')
        self.add_float(self.Events, "run")
        self.add_float(self.Events, "luminosityBlock")
        self.add_float(self.Events, "event")
        self.add_float(self.Events, "num_top_total")

    def add_float(self, tree, name, dtype=np.dtype(float)):
        if hasattr(self, name):
            print('ERROR! SetBranchAddress of name "%s" already exists!' % (name))
            exit(1)
        setattr(self, name, np.full((1), -99999999999999999999999999999999999999999999999999, dtype=dtype)) #1 elem w/ inizialization '-99999999999999999999999999999999999999999999999999'
        tree.Branch(name, getattr(self,name), '{0}/D'.format(name)) #The types in root (/D in this example) are defined here https://root.cern/root/html528/TTree.html

