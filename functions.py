#!/usr/bin/env python3
#!/usr/bin/env python3
import ROOT
import math
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection

def select_muons(event, selected_idx):
    muons = Collection(event, 'Muon')
    for imu in range(event.nMuon):
        if not event.Muon_pt[imu] >= 20: continue
        if not abs(event.Muon_eta[imu]) <= 2.4: continue
        if not event.Muon_tightId[imu]: continue
        if not event.Muon_pfRelIso04_all[imu] <= 0.15: continue
        selected_idx['muon'].append(imu)

def select_electrons(event, selected_idx):
    electrons = Collection(event, 'Electron')
    muons = Collection(event, 'Muon')
    for iele in range(event.nElectron):
        if not event.Electron_pt[iele] >= 20: continue
        if not abs(event.Electron_eta[iele]) <= 2.5: continue
        if not event.Electron_cutBased[iele] >= 1: continue
        overlap = check_overlap(selected_idx['muon'], muons, electrons[iele].p4(), 0.4)
        if overlap: continue
        selected_idx['electron'].append(iele)

def is_overlap(objectA_p4, objectB_p4, value):
    if objectA_p4.DeltaR(objectB_p4)>value: return False
    else: return True

def check_overlap(objectA_list, objectA_name, objectB_p4, value):
    overlap = False
    for imu in range(len(objectA_list)):
        overlap = is_overlap(objectA_name[objectA_list[imu]].p4(), objectB_p4, value)
        if (overlap == True): break
    return overlap

def select_top_jets(event, year, selected_idx):
    fatjets = Collection(event, 'FatJet')
    muons = Collection(event, 'Muon')
    electrons = Collection(event, 'Electron')
    for ifatjet in range(event.nFatJet):
        # Kinematic
        if not event.FatJet_pt[ifatjet] >= 200: continue
        if not abs(event.FatJet_eta[ifatjet]) < 2.4: continue #This is the recommendation for all the fat jets (there are not reconstructed forward fat jets)
        # ID
        jet_id = 10000 #Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/JetID#Recommendations_for_13_TeV_data and https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL ("Please note: For AK8 jets, the corresponding (CHS or PUPPI) AK4 jet ID should be used.")
        if year == '2018': jet_id = 2  # tight ID
        if year == '2017': jet_id = 2  # tight ID
        if year == '2016' or year == '2016APV': jet_id = 3  # tight ID
        if not event.FatJet_jetId[ifatjet] >= jet_id: continue
        # Cleaning
        overlap_mu = check_overlap(selected_idx['muon'], muons, fatjets[ifatjet].p4(), 0.8)
        if overlap_mu: continue
        overlap_e = check_overlap(selected_idx['electron'], electrons, fatjets[ifatjet].p4(), 0.8)
        if overlap_e: continue
        # Top tagged jet
        if not event.FatJet_pt[ifatjet] >= 300: continue
        pNet_id = 10000 #Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/ParticleNetTopWSFs
        if year == '2018': pNet_id = 0.58  # loose ID
        if year == '2017': pNet_id = 0.58  # loose ID
        if year == '2016': pNet_id = 0.50  # loose ID
        if year == '2016APV': pNet_id = 0.49  # loose ID
        if not event.FatJet_particleNet_TvsQCD[ifatjet] >= pNet_id: continue
        selected_idx['jetsTop_L'].append(ifatjet)
        pNet_id = 10000 #Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/ParticleNetTopWSFs
        if year == '2018': pNet_id = 0.80  # medium ID
        if year == '2017': pNet_id = 0.80  # medium ID
        if year == '2016': pNet_id = 0.73  # medium ID
        if year == '2016APV': pNet_id = 0.74  # medium ID
        if not event.FatJet_particleNet_TvsQCD[ifatjet] >= pNet_id: continue
        selected_idx['jetsTop_M'].append(ifatjet)
        pNet_id = 10000 #Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/ParticleNetTopWSFs
        if year == '2018': pNet_id = 0.97  # tight ID
        if year == '2017': pNet_id = 0.97  # tight ID
        if year == '2016': pNet_id = 0.96  # tight ID
        if year == '2016APV': pNet_id = 0.96 #Tight ID
        if not event.FatJet_particleNet_TvsQCD[ifatjet] >= pNet_id: continue
        selected_idx['jetsTop_T'].append(ifatjet)

def select_w_jets(event, year, selected_idx):
    fatjets = Collection(event, 'FatJet')
    muons = Collection(event, 'Muon')
    electrons = Collection(event, 'Electron')
    for ifatjet in range(event.nFatJet):
        # Kinematic
        if not event.FatJet_pt[ifatjet] >= 200: continue
        if not abs(event.FatJet_eta[ifatjet]) < 2.4: continue  # This is the recommendation for all the fat jets (there are not reconstructed forward fat jets)
        # ID
        jet_id = 10000  # Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/JetID#Recommendations_for_13_TeV_data and https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL ("Please note: For AK8 jets, the corresponding (CHS or PUPPI) AK4 jet ID should be used.")
        if year == "2018": jet_id = 2  # tight ID
        if year == "2017": jet_id = 2  # tight ID
        if year == "2016" or year == "2016APV": jet_id = 3  # tight ID
        if not event.FatJet_jetId[ifatjet] >= jet_id: continue
        # Cleaning
        overlap_mu = check_overlap(selected_idx['muon'], muons, fatjets[ifatjet].p4(), 0.8)
        if overlap_mu: continue
        overlap_e = check_overlap(selected_idx['electron'], electrons, fatjets[ifatjet].p4(), 0.8)
        if overlap_e: continue
        overlap_top = check_overlap(selected_idx['jetsTop_M'], fatjets, fatjets[ifatjet].p4(), 0.8)
        if overlap_top: continue
        # W tagged jet
        pNet_id = 10000  # Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/ParticleNetTopWSFs
        if year == "2018": pNet_id = 0.70  # loose ID
        if year == "2017": pNet_id = 0.71  # loose ID
        if year == "2016": pNet_id = 0.67  # loose ID
        if year == "2016APV": pNet_id = 0.68  # loose ID
        if not event.FatJet_particleNet_WvsQCD[ifatjet] >= pNet_id: continue
        selected_idx['jetsW_L'].append(ifatjet)
        pNet_id = 10000  # Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/ParticleNetTopWSFs
        if year == "2018": pNet_id = 0.94 #Medium ID
        if year=="2017": pNet_id = 0.94 #Medium ID 
        if year=="2016": pNet_id = 0.93 #Medium ID
        if year=="2016APV": pNet_id = 0.94 #Medium ID
        if not event.FatJet_particleNet_WvsQCD[ifatjet] >= pNet_id: continue
        selected_idx['jetsW_M'].append(ifatjet)
        pNet_id = 10000 #Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/ParticleNetTopWSFs
        if year=="2018": pNet_id = 0.98 #Tight ID 
        if year=="2017": pNet_id = 0.98 #Tight ID 
        if year=="2016": pNet_id = 0.97 #Tight ID
        if year=="2016APV": pNet_id = 0.97 #Tight ID
        if not event.FatJet_particleNet_WvsQCD[ifatjet] >= pNet_id: continue
        selected_idx['jetsW_T'].append(ifatjet)

def select_jets(event, year, selected_idx):
    muons = Collection(event, 'Muon')
    electrons = Collection(event, 'Electron')
    fatjets = Collection(event, 'FatJet')
    jets = Collection(event, 'Jet')
    for ijet in range(event.nJet):
        # kinematic
        if not event.Jet_pt[ijet] >= 20: continue
        jet_eta = -1
        if year == "2016" or year == "2016APV": jet_eta = 2.4
        if year == "2017": jet_eta = 2.5
        if year == "2018": jet_eta = 2.5
        if not abs(event.Jet_eta[ijet]) <= jet_eta: continue
        # ID
        jet_id = 10000  # Recommendation in https://twiki.cern.ch/twiki/bin/view/CMS/JetID#Recommendations_for_13_TeV_data and https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL ("Please note: For AK8 jets, t    he corresponding (CHS or PUPPI) AK4 jet ID should be used.")
        if year == "2016" or year == "2016APV": jet_id = 3  # tight ID
        if year == "2017": jet_id = 2  # tight ID
        if year == "2018": jet_id = 2  # tight ID
        if not event.Jet_jetId[ijet] >= jet_id: continue
        # cleaning
        overlap_mu = check_overlap(selected_idx['muon'], muons, jets[ijet].p4(), 0.4)
        if overlap_mu: continue
        overlap_e = check_overlap(selected_idx['electron'], electrons, jets[ijet].p4(), 0.4)
        if overlap_e: continue
        selected_idx['jets_noCleaned_against_boostedJets'].append(ijet)  # all the AK4 jets contain boosted jet
        overlap_top = check_overlap(selected_idx['jetsTop_M'], fatjets, jets[ijet].p4(), 0.8)
        if overlap_top: continue
        overlap_w = check_overlap(selected_idx['jetsW_M'], fatjets, jets[ijet].p4(), 0.8)
        if overlap_w: continue
        # save idx
        selected_idx['jets_cleaned'].append(ijet)  # used in the top reconstruction part for the resolved hadronic decay
        # b-jet
        bjet_eta = -1
        if year=="2016" or year=="2016APV": bjet_eta = 2.4
        if year=="2017": bjet_eta = 2.5
        if year=="2018": bjet_eta = 2.5
        if abs(event.Jet_eta[ijet]) <= bjet_eta: 
            #loose working point 
            bjet_id = 10000 #Recommendation in https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation
            if year=="2016APV": bjet_id = 0.0508 #L,M,T: 0.0508,0.2598,0.6502 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16preVFP   
            if year=="2016": bjet_id = 0.0480 #L,M,T: 0.0480,0.2489,0.6377 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16postVFP
            if year=="2017": bjet_id = 0.0532 #L,M,T: 0.0532,0.3040,0.7476 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL17
            if year=="2018": bjet_id = 0.0490 #L,M,T: 0.0490,0.2783,0.7100 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL18
            if not event.Jet_btagDeepFlavB[ijet] >= bjet_id: continue
            selected_idx['jetsB_L'].append(ijet)
            #medium working point 
            bjet_id = 10000
            if year=="2016APV": bjet_id = 0.2598 #L,M,T: 0.0508,0.2598,0.6502 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16preVFP   
            if year=="2016": bjet_id = 0.2489 #L,M,T: 0.0480,0.2489,0.6377 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16postVFP
            if year=="2017": bjet_id = 0.3040 #L,M,T: 0.0532,0.3040,0.7476 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL17
            if year=="2018": bjet_id = 0.2783 #L,M,T: 0.0490,0.2783,0.7100 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL18
            if not event.Jet_btagDeepFlavB[ijet] >= bjet_id: continue
            selected_idx['jetsB_M'].append(ijet)
            #tight working point
            bjet_id = 10000
            if year=="2016APV": bjet_id = 0.6502 #L,M,T: 0.0508,0.2598,0.6502 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16preVFP   
            if year=="2016": bjet_id = 0.6377 #L,M,T: 0.0480,0.2489,0.6377 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16postVFP
            if year=="2017": bjet_id = 0.7476 #L,M,T: 0.0532,0.3040,0.7476 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL17
            if year=="2018": bjet_id = 0.7100 #L,M,T: 0.0490,0.2783,0.7100 see https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL18
            if not event.Jet_btagDeepFlavB[ijet] >= bjet_id: continue
            selected_idx['jetsB_T'].append(ijet)
