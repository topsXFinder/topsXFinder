#!/usr/bin/env python3
import os, sys
from importlib import import_module
from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
from argparse import ArgumentParser
#This takes care of converting the input files from CRAB. It is the reason for which you need the file PSet.py also in the python directory.
from PhysicsTools.NanoAODTools.postprocessing.framework.crabhelper import inputFiles,runsAndLumis

#User inputs
parser = ArgumentParser()
parser.add_argument('-p', '--process', dest='process', action='store', choices=['local','queue','crab'], default='local')
parser.add_argument('-dt', '--dataType', dest='dataType', action='store', choices=['data','mc'], default='mc')
parser.add_argument('-y', '--year', dest='year', action='store', choices=['2016','2016APV','2017','2018'],type=str, default='2017')
parser.add_argument('-ne', '--maxNumEvt', dest='maxNumEvt', action='store', type=int, default=-1)
parser.add_argument('-pe', '--prescaleEvt', dest='prescaleEvt', action='store', type=int, default=1)
parser.add_argument('-if', '--inputFile', dest='inputFile', action='store', type=str, default='None')
args = parser.parse_args()
process = args.process
dataType = args.dataType
year = args.year
maxNumEvt = args.maxNumEvt
prescaleEvt = args.prescaleEvt
inputFile = args.inputFile
kwargs = {
    'dataType': dataType,
    'year': year,
    'maxNumEvt': maxNumEvt, #It is the maximum number of events you want to analyze. -1 means all entries from the input file. 
    'prescaleEvt': prescaleEvt, #It allows to analyze 1 event every N. 1 means analyze all events.
}

#Modules
from selector import *
module2run = lambda : Producer(**kwargs)

#Input files
if inputFile is 'None':
    if dataType=='data':
        if year=='2017':
            infiles = ['root://cms-xrd-global.cern.ch//store/data/Run2017B/SingleMuon/NANOAOD/UL2017_MiniAODv2_NanoAODv9-v1/120000/09FD9FD6-A164-9A45-80BB-F3D1FBF9C462.root'
                      ]
        elif year=='2016':
            infiles = ['root://cms-xrd-global.cern.ch//store/data/Run2016F/SingleMuon/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/7F5791FE-262B-EA45-BE19-04E4F21FEE1E.root'
                      ] 
        else:
            raise ValueError('"year" must be above 2016 (included).') 
    elif dataType=='mc':
        if year=='2017':
            infiles = ['root://cms-xrd-global.cern.ch//store/mc/RunIISummer20UL17NanoAODv9/TTTT_TuneCP5_13TeV-amcatnlo-pythia8/NANOAODSIM/106X_mc2017_realistic_v9-v2/260000/02151677-7D45-7940-803F-3FA5A101D504.root'
                      ]
        elif year=='2016':
            infiles = ['root://cms-xrd-global.cern.ch//store/mc/RunIISummer20UL16NanoAODv9/TTTT_TuneCP5_13TeV-amcatnlo-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_v17-v2/100000/09B1B183-B4C8-994A-A86B-4BBDBF2FAFDD.root'
                      ]      
        else:
            raise ValueError('"year" must be above 2016 (included).') 
    else:
        raise ValueError('"dataType" must be "data" or "mc".')
else:
    infiles = []
    infiles.append(inputFile)
 
#JSON files for data
jsonfile = os.environ['CMSSW_BASE'] + "/src/ttzp-picoframework/data/json/" 
if year=='2018': jsonfile = jsonfile+"Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt"
elif year=='2017': jsonfile = jsonfile+"Cert_294927-306462_13TeV_UL2017_Collisions17_GoldenJSON.txt"
elif year=='2016': jsonfile = jsonfile+"Cert_271036-284044_13TeV_Legacy2016_Collisions16_JSON.txt"
elif year=='2016APV': jsonfile = jsonfile+"Cert_271036-284044_13TeV_Legacy2016_Collisions16_JSON.txt"
else: raise ValueError('"year" must be above 2016 (included).')

#Run
#All options
#PostProcessor(outputDir,inputFiles,cut=None,branchsel=None,modules=[],compression="LZMA:9",friend=False,postfix=None,jsonInput=None,noOut=False,justcount=False,provenance=False,haddFileName=None,fwkJobReport=False,histFileName=None,histDirName=None,outputbranchsel=None)
if dataType=='data':
    if process=='local': p = PostProcessor(outputDir=".",noOut=True,modules=[module2run()],inputFiles=infiles)#,jsonInput=jsonfile) #No need jsonInput locally (it takes sometime to prefilter evt)
    if process=='queue': p = PostProcessor(outputDir=".",noOut=True,modules=[module2run()],inputFiles=infiles,jsonInput=jsonfile)
    if process=='crab':  p = PostProcessor(outputDir=".",noOut=True,modules=[module2run()],inputFiles=inputFiles(),jsonInput=jsonfile,fwkJobReport=True)
elif dataType=='mc':
    if process=='local': p = PostProcessor(outputDir=".",noOut=True,modules=[module2run()],inputFiles=infiles)
    if process=='queue': p = PostProcessor(outputDir=".",noOut=True,modules=[module2run()],inputFiles=infiles)
    if process=='crab':  p = PostProcessor(outputDir=".",noOut=True,modules=[module2run()],inputFiles=inputFiles(),fwkJobReport=True)
else:
    raise ValueError('"dataType" must be "data" or "mc".')
p.run()
print "DONE"

