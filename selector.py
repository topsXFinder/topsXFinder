#!/usr/bin/env python3
import os, sys
import ROOT
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

from functions import *
from variables import *
from topsXFinder import *

class declareVariables(variables):
    def __init__(self, name):
        super(declareVariables, self).__init__(name)

class Producer(Module):
    def __init__(self, **kwargs):
        #User inputs
        self.isData = 'data' in kwargs.get('dataType')
        self.year = kwargs.get('year') 
        self.maxNumEvt = kwargs.get('maxNumEvt')
        self.prescaleEvt = kwargs.get('prescaleEvt')
        #ID
  
        #Corrections

        #Cut flow table
        
    def beginJob(self):
        print "Here is beginJob"
        #pass
        
    def endJob(self):
        print "Here is endJob"
        #pass
        
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        print "Here is beginFile"
        self.sumNumEvt = 0
        self.sumgenWeight = 0
        self.out = declareVariables(inputFile) 
        #pass
        
    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):        
        print "Here is endFile"
        self.out.sumNumEvt[0] = self.sumNumEvt
        self.out.sumgenWeight[0] = self.sumgenWeight
        self.out.evtree.Fill()
        self.out.outputfile.Write()
        self.out.outputfile.Close()
        #pass
        
    def analyze(self, event):
        """process event, return True (go to next module) or False (fail, go to next event)"""
        #For all events
        if(self.sumNumEvt>self.maxNumEvt and self.maxNumEvt!=-1): return False
        self.sumNumEvt = self.sumNumEvt+1
        if not self.isData: self.sumgenWeight = self.sumgenWeight+(event.genWeight/abs(event.genWeight))
        if not self.sumNumEvt%self.prescaleEvt==0: return False
  
        selected_idx = {'muon': [], 'electron': [], 'jetsTop_L': [], 'jetsTop_M': [], 'jetsTop_T': [], 'jetsW_L': [], 'jetsW_M': [], 'jetsW_T': [], 'jets_cleaned': [], 'jets_noCleaned_against_boostedJets': [], 'jetsB_L': [], 'jetsB_M': [], 'jetsB_T': []}
        muons = Collection(event, 'Muon')
        select_muons(event, selected_idx)

        eles = Collection(event, 'Electron')
        select_electrons(event, selected_idx)

        fatjets = Collection(event, 'FatJet')
        select_top_jets(event, self.year, selected_idx)
        select_w_jets(event, self.year, selected_idx)
  
        jets = Collection(event, 'Jet')
        select_jets(event, self.year, selected_idx)

        if not ((len(selected_idx['muon'])+len(selected_idx['electron']))==1): 
            return False

        #top reconstruction
        #these are for the total results
        results ={'top':[], 'w':[], 'top_topology_decay':[], 'chi2':[]}#Top to keep all the Top(TLorentzVector), W to keep all the W(TLorentzVector) (boosted top part,just using (0,0,0,0) instead)
        #topTopologyDecay to define the kind of top (0: boosted, 1: partially-boosted, 2: resolved hadronic, 3: resolved leptonic) 
        top_type = {'boosted_top': True, 'partially_boosted_top': True, 'hadronic_top': True, 'leptonic_top': True}
        topsXFinder(event, selected_idx, top_type, results)

        #Event
        self.out.run[0] = event.run
        self.out.luminosityBlock[0] = event.luminosityBlock
        self.out.event[0] = event.event #& 0xffffffffffffffff
        self.out.num_top_total[0] = len(results['top'])
        
        #Save tree
        self.out.Events.Fill() 
        return True
