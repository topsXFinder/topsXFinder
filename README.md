# topsXFinder
reconstruct 4 kinds of tops:

    1. Fully boosted top

    2. Partially boosted top: boosted W + b jet

    3. Resolved hadronic top: 2 AK4 jet +b jet

    4. Resolved leptonic top: 1 lepton(e/mu) + MET + b jet

We only focus on the 1 lepton final state, so that we can use MET to reconsturct the W


## functions.py
In this code, we define some basic objetcs: 

    Muon, Electron, Top jet, W jet, AK4 jet, b jet

## topXFinder.py
This code is the main code to reconstruct tops according to these 4 kinds.

## selector.py
This is an example to show one basic selection: 1 lepton.

## variables.py
This code is used to keep these variables we want and define the output root file

## runner.py
This one is the code for running, here I just using a TTTT sample as an example

# How to run it?
1. copy all these files to your selection folder

2. cmsenv 

3. to run it: **python runner.py**, then you can get the result in your folder




